# RPG Heroes

This is a Console Application using basic C#.

## Description

This application is a RPG Hero selection with various hero classes, having attributes which increase at different rates as the character gains levels. The heroes can equip items such as armor and weapons which alters the power of the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item types.

## Install

Use git to clone the repository and run it locally:

```
git clone git@gitlab.com:hhoan/rpg-heroes.git
```

## Usage
After cloning the repository with git you can run the project locally.

## Contributors

[Ha Hoang](https://gitlab.com/hhoan)

