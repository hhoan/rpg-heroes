using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;
using RPG_Heroes.Heroes.Exceptions;
using RPG_Heroes.Heroes.HeroClasses;

namespace RPGHeroesTests
{
    public class RPGHeroesTests
    {
        #region Create Hero Instantiate Equipment list

        [Fact]
        public void CreateMage_InstantiateEquipmentSlotsWithNullValues_ShouldCreateListWithSlotKeysAndNullValues()
        {
            // Arrange
            Mage TestHero = new Mage("TestHero");
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestHero.LevelUp();
            Dictionary<Slot, Item?> actual = TestHero.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRanger_InstantiateEquipmentSlotsWithNullValues_ShouldCreateListWithSlotKeysAndNullValues()
        {
            // Arrange
            Ranger TestHero = new Ranger("TestHero");
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestHero.LevelUp();
            Dictionary<Slot, Item?> actual = TestHero.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRogue_InstantiateEquipmentSlotsWithNullValues_ShouldCreateListWithSlotKeysAndNullValues()
        {
            // Arrange
            Rogue TestHero = new Rogue("TestHero");
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestHero.LevelUp();
            Dictionary<Slot, Item?> actual = TestHero.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateWarrior_InstantiateEquipmentSlotsWithNullValues_ShouldCreateListWithSlotKeysAndNullValues()
        {
            // Arrange
            Warrior TestHero = new Warrior("TestHero");
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestHero.LevelUp();
            Dictionary<Slot, Item?> actual = TestHero.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region Create Hero instantiate LevelAttribute

        [Fact]
        public void CreateMage_SetLevelAttributesToHeroBaseLevel_ShouldStoreHeroBaseLevelOneInLevelAttributes()
        {
            // Arrange
            Mage TestHero = new Mage("TestHero");
            HeroAttribute expected = new(1, 1, 8);
            // Act
            HeroAttribute actual = TestHero.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        public void CreateRanger_SetLevelAttributesToHeroBaseLevel_ShouldStoreHeroBaseLevelOneInLevelAttributes()
        {
            // Arrange
            Ranger TestHero = new Ranger("TestHero");
            HeroAttribute expected = new(1, 7, 1);
            // Act
            HeroAttribute actual = TestHero.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRogue_SetLevelAttributesToHeroBaseLevel_ShouldStoreHeroBaseLevelOneInLevelAttributes()
        {
            // Arrange
            Rogue TestHero = new Rogue("TestHero");
            HeroAttribute expected = new(2, 6, 1);
            // Act
            HeroAttribute actual = TestHero.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateWarrior_SetLevelAttributesToHeroBaseLevel_ShouldStoreHeroBaseLevelOneInLevelAttributes()
        {
            // Arrange
            Warrior TestHero = new Warrior("TestHero");
            HeroAttribute expected = new(5, 2, 1);
            // Act
            HeroAttribute actual = TestHero.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }


        #endregion

        #region Create Hero Instantiate ValidWeaponTypes

        [Fact]
        public void CreateMage_AddValidWeaponTypesToList_ShouldCreateListWithValidWeaponTypes()
        {
            // Arrange
            Mage TestHero = new Mage("TestHero");
            List<WeaponType> expected = new List<WeaponType> { WeaponType.Staff, WeaponType.Wand};
            // Act
            TestHero.LevelUp();
            List<WeaponType> actual = TestHero.ValidWeaponTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRanger_AddValidWeaponTypesToList_ShouldCreateListWithValidWeaponTypes()
        {
            // Arrange
            Ranger TestHero = new Ranger("TestHero");
            List<WeaponType> expected = new List<WeaponType> { WeaponType.Bow };
            // Act
            TestHero.LevelUp();
            List<WeaponType> actual = TestHero.ValidWeaponTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void CreateRogue_AddValidWeaponTypesToList_ShouldCreateListWithValidWeaponTypes()
        {
            // Arrange
            Rogue TestHero = new Rogue("TestHero");
            List<WeaponType> expected = new List<WeaponType> { WeaponType.Dagger, WeaponType.Sword };
            // Act
            TestHero.LevelUp();
            List<WeaponType> actual = TestHero.ValidWeaponTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        [Fact]
        public void CreateWarrior_AddValidWeaponTypesToList_ShouldCreateListWithValidWeaponTypes()
        {
            // Arrange
            Warrior TestHero = new Warrior("TestHero");
            List<WeaponType> expected = new List<WeaponType> { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
            // Act
            TestHero.LevelUp();
            List<WeaponType> actual = TestHero.ValidWeaponTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region Create Hero Instantiate ValidArmorTypes

        [Fact]
        public void CreateMage_AddValidArmorTypesToList_ShouldCreateListWithValidArmorTypes()
        {
            // Arrange
            Mage TestHero = new Mage("TestHero");
            List<ArmorType> expected = new List<ArmorType> { ArmorType.Cloth };
            // Act
            TestHero.LevelUp();
            List<ArmorType> actual = TestHero.ValidArmorTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRanger_AddValidArmorTypesToList_ShouldCreateListWithValidArmorTypes()
        {
            // Arrange
            Ranger TestHero = new Ranger("TestHero");
            List<ArmorType> expected = new List<ArmorType> { ArmorType.Leather, ArmorType.Mail };
            // Act
            TestHero.LevelUp();
            List<ArmorType> actual = TestHero.ValidArmorTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void CreateRogue_AddValidArmorTypesToList_ShouldCreateListWithValidArmorTypes()
        {
            // Arrange
            Rogue TestHero = new Rogue("TestHero");
            List<ArmorType> expected = new List<ArmorType> { ArmorType.Leather, ArmorType.Mail };
            // Act
            TestHero.LevelUp();
            List<ArmorType> actual = TestHero.ValidArmorTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

       
        [Fact]
        public void CreateWarrior_AddValidArmorTypesToList_ShouldCreateListWithValidArmorTypes()
        {
            // Arrange
            Warrior TestHero = new Warrior("TestHero");
            List<ArmorType> expected = new List<ArmorType> { ArmorType.Mail, ArmorType.Plate };
            // Act
            TestHero.LevelUp();
            List<ArmorType> actual = TestHero.ValidArmorTypes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region LevelUp Increase Level
        [Fact]
        public void LevelUpMage_IncreaseLevel_ShouldIncreaseLevelByOne()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            int Level = 1;
            int expected = Level + 1;
            // Act
            TestMage.LevelUp();
            int actual = TestMage.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpRanger_IncreaseLevel_ShouldIncreaseLevelByOne()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            int Level = 1;
            int expected = Level + 1;
            // Act
            TestRanger.LevelUp();
            int actual = TestRanger.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpRogue_IncreaseLevel_ShouldIncreaseLevelByOne()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            int Level = 1;
            int expected = Level + 1;
            // Act
            TestRogue.LevelUp();
            int actual = TestRogue.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpWarrior_IncreaseLevel_ShouldIncreaseLevelByOne()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            int Level = 1;
            int expected = Level + 1;
            // Act
            TestWarrior.LevelUp();
            int actual = TestWarrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region LevelUp Increase LevelAttributes
        [Fact]
        public void LevelUpMage_IncreaseLevelAttributes_ShouldIncreaseLevelAttributes()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            HeroAttribute LevelOneAttributes = new HeroAttribute(1, 1, 8);
            HeroAttribute IncreaseBy = new HeroAttribute(1, 1, 5);
            HeroAttribute expected = new HeroAttribute(LevelOneAttributes.Strength + IncreaseBy.Strength, LevelOneAttributes.Dexterity + IncreaseBy.Dexterity, LevelOneAttributes.Intelligence + IncreaseBy.Intelligence);
            // Act
            TestMage.LevelUp();
            HeroAttribute actual = TestMage.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUpRanger_IncreaseLevelAttributes_ShouldIncreaseLevelAttributes()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            HeroAttribute LevelOneAttributes = new HeroAttribute(1, 7, 1);
            HeroAttribute IncreaseBy = new HeroAttribute(1, 5, 1);
            HeroAttribute expected = new HeroAttribute(LevelOneAttributes.Strength + IncreaseBy.Strength, LevelOneAttributes.Dexterity + IncreaseBy.Dexterity, LevelOneAttributes.Intelligence + IncreaseBy.Intelligence);
            // Act
            TestRanger.LevelUp();
            HeroAttribute actual = TestRanger.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUpRogue_IncreaseLevelAttributes_ShouldIncreaseLevelAttributes()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            HeroAttribute LevelOneAttributes = new HeroAttribute(2, 6, 1);
            HeroAttribute IncreaseBy = new HeroAttribute(1, 4, 1);
            HeroAttribute expected = new HeroAttribute(LevelOneAttributes.Strength + IncreaseBy.Strength, LevelOneAttributes.Dexterity + IncreaseBy.Dexterity, LevelOneAttributes.Intelligence + IncreaseBy.Intelligence);
            // Act
            TestRogue.LevelUp();
            HeroAttribute actual = TestRogue.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUpWarrior_IncreaseLevelAttributes_ShouldIncreaseLevelAttributes()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            HeroAttribute LevelOneAttributes = new HeroAttribute(5, 2, 1);
            HeroAttribute IncreaseBy = new HeroAttribute(3, 2, 1);
            HeroAttribute expected = new HeroAttribute(LevelOneAttributes.Strength + IncreaseBy.Strength, LevelOneAttributes.Dexterity + IncreaseBy.Dexterity, LevelOneAttributes.Intelligence + IncreaseBy.Intelligence);
            // Act
            TestWarrior.LevelUp();
            HeroAttribute actual = TestWarrior.LevelAttributes;
            // Assert
            Assert.Equivalent(expected, actual);
        }
        #endregion

        #region Equip valid weapon

        [Theory]
        [InlineData(WeaponType.Staff)]
        [InlineData(WeaponType.Wand)]
        public void EquipWeaponMage_AddValidWeaponToWeaponSlot_ShouldUpdateTheValueWithTheWeapon(WeaponType weaponType)
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, weaponType, 2);
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, TestWeapon },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestMage.Equip(TestWeapon);
            Dictionary<Slot, Item?> actual = TestMage.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void EquipWeaponRanger_AddValidWeaponToWeaponSlot_ShouldUpdateTheValueWithTheWeapon()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Bow, 2);
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, TestWeapon },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestRanger.Equip(TestWeapon);
            Dictionary<Slot, Item?> actual = TestRanger.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Theory]
        [InlineData(WeaponType.Dagger)]
        [InlineData(WeaponType.Sword)]
        public void EquipWeaponRogue_AddValidWeaponToWeaponSlot_ShouldUpdateTheValueWithTheWeapon(WeaponType weaponType)
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, weaponType, 2);
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, TestWeapon },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestRogue.Equip(TestWeapon);
            Dictionary<Slot, Item?> actual = TestRogue.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Theory]
        [InlineData(WeaponType.Sword)]
        [InlineData(WeaponType.Hammer)]
        [InlineData(WeaponType.Axe)]
        public void EquipWeaponWarrior_AddValidWeaponToWeaponSlot_ShouldUpdateTheValueWithTheWeapon(WeaponType weaponType)
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, weaponType, 2);
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, TestWeapon },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            // Act
            TestWarrior.Equip(TestWeapon);
            Dictionary<Slot, Item?> actual = TestWarrior.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region Equip Invalid WeaponType


        [Fact]
        public void EquipWeaponMage_AddInvalidWeaponTypeToWeaponSlot_ShouldThrowInvalidWeaponException() 
        { 
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Axe, 2);   
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestMage.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponRanger_AddInvalidWeaponTypeToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Axe, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestRanger.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponRogue_AddInvalidWeaponTypeToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Axe, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestRogue.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponWarrior_AddInvalidWeaponTypeToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Staff, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestWarrior.Equip(TestWeapon));
        }

        #endregion

        #region Equip Invalid Weapon RequiredLevel > Level


        [Fact]
        public void EquipWeaponMage_AddInvalidWeaponWithInvalidRequiredLevelToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Weapon TestWeapon = new Weapon("TestWeapon", 2, WeaponType.Staff, 2);

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestMage.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponRanger_AddInvalidWeaponWithTooHighLevelRequirementToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Weapon TestWeapon = new Weapon("TestWeapon", 2, WeaponType.Bow, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestRanger.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponRogue_AddInvalidWeaponWithTooHighLevelRequirementToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Weapon TestWeapon = new Weapon("TestWeapon", 2, WeaponType.Dagger, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestRogue.Equip(TestWeapon));
        }

        [Fact]
        public void EquipWeaponWarrior_AddInvalidWeaponWithTooHighLevelRequirementToWeaponSlot_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Weapon TestWeapon = new Weapon("TestWeapon", 2, WeaponType.Axe, 2);
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => TestWarrior.Equip(TestWeapon));
        }

        #endregion

        #region Equip valid armor

        [Fact]
        public void EquipArmorMage_AddValidArmorToArmorSlot_ShouldUpdateTheValueWithTheArmor()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, ArmorType.Cloth, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, ArmorType.Cloth, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, ArmorType.Cloth, new(1, 1, 1));
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, TestArmorHead },
                { Slot.Body, TestArmorBody },
                { Slot.Legs, TestArmorLegs }
            };
            // Act
            TestMage.Equip(TestArmorHead);
            TestMage.Equip(TestArmorBody);
            TestMage.Equip(TestArmorLegs);
            Dictionary<Slot, Item?> actual = TestMage.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Theory]
        [InlineData(ArmorType.Leather)]
        [InlineData(ArmorType.Mail)]
        public void EquipArmorRanger_AddValidArmorToArmorSlot_ShouldUpdateTheValueWithTheArmor(ArmorType armorType)
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, armorType, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, armorType, new(1, 1, 1));
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, TestArmorHead },
                { Slot.Body, TestArmorBody },
                { Slot.Legs, TestArmorLegs }
            };
            // Act
            TestRanger.Equip(TestArmorHead);
            TestRanger.Equip(TestArmorBody);
            TestRanger.Equip(TestArmorLegs);
            Dictionary<Slot, Item?> actual = TestRanger.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Theory]
        [InlineData(ArmorType.Leather)]
        [InlineData(ArmorType.Mail)]
        public void EquipArmorRogue_AddValidArmorToArmorSlot_ShouldUpdateTheValueWithTheArmor(ArmorType armorType)
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, armorType, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, armorType, new(1, 1, 1));
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, TestArmorHead },
                { Slot.Body, TestArmorBody },
                { Slot.Legs, TestArmorLegs }
            };
            // Act
            TestRogue.Equip(TestArmorHead);
            TestRogue.Equip(TestArmorBody);
            TestRogue.Equip(TestArmorLegs);
            Dictionary<Slot, Item?> actual = TestRogue.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Theory]
        [InlineData(ArmorType.Plate)]
        [InlineData(ArmorType.Mail)]
        public void EquipArmorWarrior_AddValidArmorToArmorSlot_ShouldUpdateTheValueWithTheArmor(ArmorType armorType)
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, armorType, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, armorType, new(1, 1, 1));
            Dictionary<Slot, Item?> expected = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, TestArmorHead },
                { Slot.Body, TestArmorBody },
                { Slot.Legs, TestArmorLegs }
            };
            // Act
            TestWarrior.Equip(TestArmorHead);
            TestWarrior.Equip(TestArmorBody);
            TestWarrior.Equip(TestArmorLegs);
            Dictionary<Slot, Item?> actual = TestWarrior.Equipment;
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region Equip Invalid ArmorType


        [Theory]
        [InlineData(ArmorType.Plate)]
        [InlineData(ArmorType.Leather)]
        [InlineData(ArmorType.Mail)]
        public void EquipArmorMage_AddInvalidArmorTypeToArmorSlot_ShouldThrowInvalidArmorException(ArmorType armorType)
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => TestMage.Equip(TestArmor));
        }

        [Theory]
        [InlineData(ArmorType.Cloth)]
        [InlineData(ArmorType.Plate)]
        public void EquipArmorRanger_AddInvalidArmorTypeToArmorSlot_ShouldThrowInvalidArmorException(ArmorType armorType)
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => TestRanger.Equip(TestArmor));
        }

        [Theory]
        [InlineData(ArmorType.Cloth)]
        [InlineData(ArmorType.Plate)]
        public void EquipArmorRogue_AddInvalidArmorTypeToArmorSlot_ShouldThrowInvalidArmorException(ArmorType armorType)
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));
            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => TestRogue.Equip(TestArmor));
        }

        [Theory]
        [InlineData(ArmorType.Cloth)]
        [InlineData(ArmorType.Leather)]
        public void EquipArmorWarrior_AddInvalidArmorTypeToArmorSlot_ShouldThrowInvalidArmorException(ArmorType armorType)
        { 
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Head, armorType, new(1, 1, 1));
            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => TestWarrior.Equip(TestArmor));
        }

        #endregion

        #region TotalAttributes with one armor equipment

        [Fact]
        public void TotalAttributesMage_CalculateTotalAttributesWithOneArmorEquipment_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Body, ArmorType.Cloth, new(1, 1, 1));
            HeroAttribute expected = new(
                TestMage.LevelAttributes.Strength + TestArmor.ArmorAttributes.Strength,
                TestMage.LevelAttributes.Dexterity + TestArmor.ArmorAttributes.Dexterity,
                TestMage.LevelAttributes.Intelligence + TestArmor.ArmorAttributes.Intelligence
                );
            // Act
            TestMage.Equip(TestArmor);
            HeroAttribute actual = TestMage.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesRanger_CalculateTotalAttributesWithOneArmorEquipment_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Body, ArmorType.Leather, new(1, 1, 1));
            HeroAttribute expected = new(
                TestRanger.LevelAttributes.Strength + TestArmor.ArmorAttributes.Strength,
                TestRanger.LevelAttributes.Dexterity + TestArmor.ArmorAttributes.Dexterity,
                TestRanger.LevelAttributes.Intelligence + TestArmor.ArmorAttributes.Intelligence
                );
            // Act
            TestRanger.Equip(TestArmor);
            HeroAttribute actual = TestRanger.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesRogue_CalculateTotalAttributesWithOneArmorEquipment_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Body, ArmorType.Leather, new(1, 1, 1));
            HeroAttribute expected = new(
                TestRogue.LevelAttributes.Strength + TestArmor.ArmorAttributes.Strength,
                TestRogue.LevelAttributes.Dexterity + TestArmor.ArmorAttributes.Dexterity,
                TestRogue.LevelAttributes.Intelligence + TestArmor.ArmorAttributes.Intelligence
                );
            // Act
            TestRogue.Equip(TestArmor);
            HeroAttribute actual = TestRogue.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesWarrior_CalculateTotalAttributesWithOneArmorEquipment_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Armor TestArmor = new Armor("TestArmor", 1, Slot.Body, ArmorType.Plate, new(1, 1, 1));
            HeroAttribute expected = new(
                TestWarrior.LevelAttributes.Strength + TestArmor.ArmorAttributes.Strength,
                TestWarrior.LevelAttributes.Dexterity + TestArmor.ArmorAttributes.Dexterity,
                TestWarrior.LevelAttributes.Intelligence + TestArmor.ArmorAttributes.Intelligence
                );
            // Act
            TestWarrior.Equip(TestArmor);
            HeroAttribute actual = TestWarrior.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }
        #endregion

        #region TotalAttributes with full armor equipment

        [Fact]
        public void TotalAttributesMage_CalculateTotalAttributesWithFullArmorEquipped_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, ArmorType.Cloth, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, ArmorType.Cloth, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, ArmorType.Cloth, new(1, 1, 1));
            
            HeroAttribute expected = new(
                TestMage.LevelAttributes.Strength + TestArmorHead.ArmorAttributes.Strength + TestArmorBody.ArmorAttributes.Strength + TestArmorLegs.ArmorAttributes.Strength,
                TestMage.LevelAttributes.Dexterity + TestArmorHead.ArmorAttributes.Dexterity + TestArmorBody.ArmorAttributes.Dexterity + TestArmorLegs.ArmorAttributes.Dexterity,
                TestMage.LevelAttributes.Intelligence + TestArmorHead.ArmorAttributes.Intelligence + TestArmorBody.ArmorAttributes.Intelligence + TestArmorLegs.ArmorAttributes.Intelligence
                );
            // Act
            TestMage.Equip(TestArmorHead);
            TestMage.Equip(TestArmorBody);
            TestMage.Equip(TestArmorLegs);
            HeroAttribute actual = TestMage.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }


        [Fact]
        public void TotalAttributesRanger_CalculateTotalAttributesWithFullArmorEquipped_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, ArmorType.Leather, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, ArmorType.Leather, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, ArmorType.Leather, new(1, 1, 1));

            HeroAttribute expected = new(
                TestRanger.LevelAttributes.Strength + TestArmorHead.ArmorAttributes.Strength + TestArmorBody.ArmorAttributes.Strength + TestArmorLegs.ArmorAttributes.Strength,
                TestRanger.LevelAttributes.Dexterity + TestArmorHead.ArmorAttributes.Dexterity + TestArmorBody.ArmorAttributes.Dexterity + TestArmorLegs.ArmorAttributes.Dexterity,
                TestRanger.LevelAttributes.Intelligence + TestArmorHead.ArmorAttributes.Intelligence + TestArmorBody.ArmorAttributes.Intelligence + TestArmorLegs.ArmorAttributes.Intelligence
                );
            // Act
            TestRanger.Equip(TestArmorHead);
            TestRanger.Equip(TestArmorBody);
            TestRanger.Equip(TestArmorLegs);
            HeroAttribute actual = TestRanger.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }


        [Fact]
        public void TotalAttributesRogue_CalculateTotalAttributesWithFullArmorEquipped_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, ArmorType.Mail, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, ArmorType.Mail, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, ArmorType.Mail, new(1, 1, 1));

            HeroAttribute expected = new(
                TestRogue.LevelAttributes.Strength + TestArmorHead.ArmorAttributes.Strength + TestArmorBody.ArmorAttributes.Strength + TestArmorLegs.ArmorAttributes.Strength,
                TestRogue.LevelAttributes.Dexterity + TestArmorHead.ArmorAttributes.Dexterity + TestArmorBody.ArmorAttributes.Dexterity + TestArmorLegs.ArmorAttributes.Dexterity,
                TestRogue.LevelAttributes.Intelligence + TestArmorHead.ArmorAttributes.Intelligence + TestArmorBody.ArmorAttributes.Intelligence + TestArmorLegs.ArmorAttributes.Intelligence
                );
            // Act
            TestRogue.Equip(TestArmorHead);
            TestRogue.Equip(TestArmorBody);
            TestRogue.Equip(TestArmorLegs);
            HeroAttribute actual = TestRogue.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }


        [Fact]
        public void TotalAttributesWarrior_CalculateTotalAttributesWithFullArmorEquipped_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Armor TestArmorHead = new Armor("TestArmor", 1, Slot.Head, ArmorType.Plate, new(1, 1, 1));
            Armor TestArmorBody = new Armor("TestArmor", 1, Slot.Body, ArmorType.Plate, new(1, 1, 1));
            Armor TestArmorLegs = new Armor("TestArmor", 1, Slot.Legs, ArmorType.Plate, new(1, 1, 1));

            HeroAttribute expected = new(
                TestWarrior.LevelAttributes.Strength + TestArmorHead.ArmorAttributes.Strength + TestArmorBody.ArmorAttributes.Strength + TestArmorLegs.ArmorAttributes.Strength,
                TestWarrior.LevelAttributes.Dexterity + TestArmorHead.ArmorAttributes.Dexterity + TestArmorBody.ArmorAttributes.Dexterity + TestArmorLegs.ArmorAttributes.Dexterity,
                TestWarrior.LevelAttributes.Intelligence + TestArmorHead.ArmorAttributes.Intelligence + TestArmorBody.ArmorAttributes.Intelligence + TestArmorLegs.ArmorAttributes.Intelligence
                );
            // Act
            TestWarrior.Equip(TestArmorHead);
            TestWarrior.Equip(TestArmorBody);
            TestWarrior.Equip(TestArmorLegs);
            HeroAttribute actual = TestWarrior.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region TotalAttributes with no armor

        [Fact]
        public void TotalAttributesMage_CalculateTotalAttributesWithNoArmorEquipment_ShouldReturnLevelAttributes()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            HeroAttribute expected = TestMage.LevelAttributes;
            // Act
            HeroAttribute actual = TestMage.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesRanger_CalculateTotalAttributesWithNoArmorEquipment_ShouldReturnLevelAttributes()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            HeroAttribute expected = TestRanger.LevelAttributes;
            // Act
            HeroAttribute actual = TestRanger.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesRogue_CalculateTotalAttributesWithNoArmorEquipment_ShouldReturnLevelAttributes()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            HeroAttribute expected = TestRogue.LevelAttributes;
            // Act
            HeroAttribute actual = TestRogue.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributesWarrior_CalculateTotalAttributesWithNoArmorEquipment_ShouldReturnLevelAttributesPlusArmorAttributes()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            HeroAttribute expected = TestWarrior.LevelAttributes;   
            // Act
            HeroAttribute actual = TestWarrior.TotalAttributes();
            // Assert
            Assert.Equivalent(expected, actual);
        }

        #endregion

        #region Damage without weapon

        [Fact]
        public void DamageMage_CalculateDamageWithoutWeapon_ShouldReturnHeroDamage()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            double damagingAttribute = TestMage.TotalAttributes().Intelligence;
            double expected = 1 * (1 + damagingAttribute / 100);
            // Act
            double actual = TestMage.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DamageRanger_CalculateDamageWithoutWeapon_ShouldReturnHeroDamage()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            double damagingAttribute = TestRanger.TotalAttributes().Dexterity;
            double expected = 1 * (1 + damagingAttribute / 100);
            // Act
            double actual = TestRanger.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void DamageRogue_CalculateDamageWithoutWeapon_ShouldReturnHeroDamage()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            double damagingAttribute = TestRogue.TotalAttributes().Dexterity;
            double expected = 1 * (1 + damagingAttribute / 100);
            // Act
            double actual = TestRogue.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DamageWarrior_CalculateDamageWithoutWeapon_ShouldReturnHeroDamage()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            double damagingAttribute = TestWarrior.TotalAttributes().Strength;
            double expected = 1 * (1 + damagingAttribute / 100);
            // Act
            double actual = TestWarrior.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }


        #endregion

        #region Damage with weapon

        [Fact]
        public void DamageMage_CalculateDamageWithWeapon_ShouldReturnTotalDamage()
        {
            // Arrange
            Mage TestMage = new Mage("TestMage");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Staff, 10);
            double damagingAttribute = TestMage.TotalAttributes().Intelligence;
            double expected = TestWeapon.WeaponDamage * (1 + damagingAttribute / 100);
            // Act
            TestMage.Equip(TestWeapon);
            double actual = TestMage.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DamageRanger_CalculateDamageWithWeapon_ShouldReturnTotalDamage()
        {
            // Arrange
            Ranger TestRanger = new Ranger("TestRanger");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Bow, 15);
            double damagingAttribute = TestRanger.TotalAttributes().Dexterity;
            double expected = TestWeapon.WeaponDamage * (1 + damagingAttribute / 100);
            // Act
            TestRanger.Equip(TestWeapon);
            double actual = TestRanger.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void DamageRogue_CalculateDamageWithWeapon_ShouldReturnTotalDamage()
        {
            // Arrange
            Rogue TestRogue = new Rogue("TestRogue");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Dagger, 20);
            double damagingAttribute = TestRogue.TotalAttributes().Dexterity;
            double expected = 1 * (1 + damagingAttribute / 100);
            // Act
            double actual = TestRogue.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DamageWarrior_CalculateDamageWithWeapon_ShouldReturnTotalDamage()
        {
            // Arrange
            Warrior TestWarrior = new Warrior("TestWarrior");
            Weapon TestWeapon = new Weapon("TestWeapon", 1, WeaponType.Axe, 25);
            double damagingAttribute = TestWarrior.TotalAttributes().Strength;
            double expected = TestWeapon.WeaponDamage * (1 + damagingAttribute / 100);
            // Act
            TestWarrior.Equip(TestWeapon);
            double actual = TestWarrior.Damage();
            // Assert
            Assert.Equal(expected, actual);
        }


        #endregion

    }
}