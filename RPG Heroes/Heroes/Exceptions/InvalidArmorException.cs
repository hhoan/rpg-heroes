﻿
namespace RPG_Heroes.Heroes.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string? message) : base(message)
        {
        }
    }
}
