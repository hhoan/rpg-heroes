﻿
namespace RPG_Heroes.Heroes.Equipment
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
