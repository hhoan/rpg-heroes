﻿using RPG_Heroes.Heroes.Equipment;

namespace RPG_Heroes.Heroes.Equipment
{
    public class Weapon : Item 
    {
        public WeaponType WeaponType;
        public int WeaponDamage { get; set; }

        public Weapon(string name, int requiredLevel, WeaponType WeaponType, int WeaponDamage) : base(name, requiredLevel)
        {
            this.WeaponType = WeaponType;
            this.WeaponDamage = WeaponDamage;
        }
    }
}
