﻿using RPG_Heroes.Heroes.Equipment;

namespace RPG_Heroes.Heroes.Equipment
{
    public abstract class Item
    {
        public string Name;
        public int RequiredLevel;
        public Slot Slot { get; set; }

        protected Item(string name, int requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = Slot.Weapon;
        }
        protected Item(string name, int requiredLevel, Slot slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
    }
}
