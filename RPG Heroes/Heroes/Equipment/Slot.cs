﻿
namespace RPG_Heroes.Heroes.Equipment
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
