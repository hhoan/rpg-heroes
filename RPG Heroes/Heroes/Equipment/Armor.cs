﻿using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;

namespace RPG_Heroes.Heroes.Equipment
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; private set; }
        public HeroAttribute ArmorAttributes { get; private set; }

        public Armor(string name, int requiredLevel, Slot slot, ArmorType ArmorType, HeroAttribute ArmorAttributes) : base(name, requiredLevel, slot)
        {
            this.ArmorType = ArmorType;
            this.ArmorAttributes = ArmorAttributes;
        }
    }
}
