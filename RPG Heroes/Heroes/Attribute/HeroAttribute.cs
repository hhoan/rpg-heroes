﻿
namespace RPG_Heroes.Heroes.Attribute
{
    public class HeroAttribute
    {

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get;  set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Calculates sum of two HeroAttributes by adding the attributes in the parameter to the first HeroAttribute.
        /// </summary>
        /// <param name="amountToIncrease">HeroAttribute modifier</param>
        public void IncreaseAttributes(HeroAttribute amountToIncrease)
        {
            Strength += amountToIncrease.Strength;
            Dexterity += amountToIncrease.Dexterity;
            Intelligence += amountToIncrease.Intelligence;
        }
    }
}
