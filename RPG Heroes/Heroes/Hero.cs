﻿using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;
using RPG_Heroes.Heroes.Exceptions;
using System.Text;

namespace RPG_Heroes.Heroes
{
    public abstract class Hero
    {

        public string Name { get; private set; }
        public int Level { get; set; }
        public HeroAttribute LevelAttributes { get; set; }
        public Dictionary<Slot, Item?> Equipment { get; set; } 
        public List<WeaponType> ValidWeaponTypes { get; set; }
        public List<ArmorType> ValidArmorTypes { get; set; }


        /// <summary>
        /// Base Constructor for Hero Classes. Declare hero properties.
        /// </summary>
        /// <param name="name">Hero name</param>
        public Hero(string name)
        {
            this.Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item?>
            {
                { Slot.Weapon, null },
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null }
            };
            LevelAttributes = new HeroAttribute(1, 1, 1);
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();
        }

        /// <summary>
        /// Increase Hero level by one.
        /// </summary>
        public virtual void LevelUp()
        {
            Level++;
            
        }

        /// <summary>
        /// Set Slot.Weapon value to weapon if it is the correct WeaponType and RequiredLevel is fulfilled. Throws custom exception message if the requirements are not met.
        /// </summary>
        /// <param name="weapon">The weapon to equip</param>
        /// <exception cref="InvalidWeaponException"></exception>
        public void Equip(Weapon weapon) {
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
                throw new InvalidWeaponException("Character can not equip this WeaponType.");

            if (weapon.RequiredLevel > Level)
                throw new InvalidWeaponException("RequiredLevel is too high.");

            Equipment[Slot.Weapon] = weapon;
        }

        /// <summary>
        /// Set the Armor Slot value to armor item if the correct ArmorType and RequiredLevel is fulfilled. Throws custom exception message if not.
        /// </summary>
        /// <param name="armor">The armor item to equip</param>
        /// <exception cref="InvalidArmorException"></exception>
        public void Equip(Armor armor) {
            if (!ValidArmorTypes.Contains(armor.ArmorType))
                throw new InvalidArmorException("Character can not equip this ArmorType.");
            if (armor.RequiredLevel > Level)
                throw new InvalidArmorException("RequiredLevel is too high.");

            Equipment[armor.Slot] = armor;
        }

        /// <summary>
        /// Returns WeaponDamage if weapon is equipped. If no weapon is equipped then set WeaponDamage to 1 and return it.
        /// </summary>
        /// <returns>WeaponDamage</returns>
        public virtual double Damage()
        {
            double weaponDamage;
            Weapon? weapon = (Weapon?)Equipment[Slot.Weapon];
            if (weapon != null)
            {
                weaponDamage = weapon.WeaponDamage;
            }
            else weaponDamage = 1;

            return weaponDamage;
        }

        /// <summary>
        /// LevelAttributes + ArmorAttributes. Loops through armor slots in equipments and adds the attributes to total.
        /// </summary>
        /// <returns>totalAttributes</returns>
        public HeroAttribute TotalAttributes() {
            HeroAttribute totalAttributes = LevelAttributes;
            foreach (var item in Equipment)
            {
                if (item.Value != null && item.Key != Slot.Weapon)
                {
                    Armor armor = (Armor)item.Value;
                    totalAttributes.IncreaseAttributes(armor.ArmorAttributes);
                }  
            }
            return totalAttributes;
        }

        /// <summary>
        /// Display Hero stats: Name, Class, Level, Total Strength, Total Dexterity, Total Intelligence, Damage
        /// </summary>
        public void Display() {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("Class: {0}", this.GetType().Name);
            Console.WriteLine("Level: {0}", Level);
            Console.WriteLine("Total Strength: {0}", TotalAttributes().Strength);
            Console.WriteLine("Total Dexterity: {0}", TotalAttributes().Dexterity);
            Console.WriteLine("Total Intelligence: {0}", TotalAttributes().Intelligence);
            Console.WriteLine("Damage: {0}", Damage());

        }

    }
}
