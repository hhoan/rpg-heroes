﻿using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Heroes.HeroClasses
{
    public class Warrior : Hero
    {
        /// <summary>
        /// Sets Level 1 LevelAttributes => str = 5, dex = 2, int = 1.
        /// Sets ValidWeaponTypes to Axe, Hammer and Sword.
        /// Sets ValidArmorTypes to Plate and Mail.
        /// </summary>
        /// <param name="name">Hero name</param>
        public Warrior(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(5, 2, 1);

            ValidWeaponTypes = new List<WeaponType>
            {
                WeaponType.Axe,
                WeaponType.Hammer,
                WeaponType.Sword
            };
            ValidArmorTypes = new List<ArmorType>
            {
                ArmorType.Mail,
                ArmorType.Plate
            };
        }

        /// <summary>
        /// Increase LevelAttributes with str=3, dex=2, int=1 when the hero levels up.
        /// </summary>
        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseAttributes(new(3, 2, 1));
        }

        /// <summary>
        /// Takes the returned weaponDamage from parent class and increase it with 1% for every point in the hero's damaging attribute.
        /// </summary>
        /// <returns>WeaponDamage with Strength modifier</returns>
        public override double Damage()
        {
            return base.Damage() * (1 + (double)TotalAttributes().Strength / 100);
        }
    }
}
