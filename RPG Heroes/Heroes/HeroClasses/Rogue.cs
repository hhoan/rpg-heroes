﻿using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Heroes.HeroClasses
{
    public class Rogue : Hero
    {

        /// <summary>
        /// Sets Level 1 LevelAttributes => str = 2, dex = 6, int = 1.
        /// Sets ValidWeaponTypes to Dagger and Sword.
        /// Sets ValidArmorTypes to Leather and Mail.
        /// </summary>
        /// <param name="name">Hero name</param>
        public Rogue(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(2, 6, 1);

            ValidWeaponTypes = new List<WeaponType>
            {
                WeaponType.Dagger,
                WeaponType.Sword
            };
            ValidArmorTypes = new List<ArmorType>
            {
                ArmorType.Leather,
                ArmorType.Mail
            };
        }

        /// <summary>
        /// Increase LevelAttributes with str=1, dex=4, int=1 when the hero levels up.
        /// </summary>
        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseAttributes(new(1, 4, 1));
        }

        /// <summary>
        /// Takes the returned weaponDamage from parent class and increase it with 1% for every point in the hero's damaging attribute.
        /// </summary>
        /// <returns>WeaponDamage with Dexterity modifier</returns>
        public override double Damage()
        {
            return base.Damage() * (1 + (double)TotalAttributes().Dexterity / 100);
        }
    }
}
