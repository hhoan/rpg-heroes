﻿using RPG_Heroes.Heroes.Attribute;
using RPG_Heroes.Heroes.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Heroes.HeroClasses
{
    public class Mage : Hero
    {

        /// <summary>
        /// Sets Level 1 LevelAttributes => str = 1, dex = 1, int = 8.
        /// Sets ValidWeaponTypes to Staff and Wand.
        /// Sets ValidArmorTypes to Cloth.
        /// </summary>
        /// <param name="name">Hero name</param>
        public Mage(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(1, 1, 8);
            ValidWeaponTypes = new List<WeaponType>
            {
                WeaponType.Staff,
                WeaponType.Wand
            };
            ValidArmorTypes = new List<ArmorType> 
            { 
                ArmorType.Cloth 
            };
        }

        /// <summary>
        /// Increase LevelAttributes with str=1, dex=1, int=5 when the hero levels up.
        /// </summary>
        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseAttributes(new(1, 1, 5));
        }

        /// <summary>
        /// Takes the returned weaponDamage from parent class and increase it with 1% for every point in the hero's damaging attribute.
        /// </summary>
        /// <returns>WeaponDamage with Intelligence modifier</returns>
        public override double Damage()
        {
            return base.Damage() * (1 + (double)TotalAttributes().Intelligence / 100);
        }

    }
}
